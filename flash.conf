[setup]
boxlength       = 1 # Mpc / h
zstart          = 500 # starting redshift
levelmin        = 8 # min refinement level (in powers of 2; e.g., 2^8 = 256)
levelmin_TF     = 8 # set >= levelmin, as close as possible to levelmax
levelmax        = 8 # max refinement level
padding         = 4 # Number of grid cells in intermediate levels (when using levelmax>levelmin+1) surrounding the nested grids. The extent of an intermediate level is N/2+2*padding if N is the number of cells in the next finer level.
overlap         = 4 # Number of extra padding cells for subgrids when computing the transfer function convolutions. These are discarded when computing the displacements but greatly reduce errors due to boundary effects
# blocking_factor         =
align_top       = yes # Require subgrids to be always aligned with the coarsest grid? This is necessary for some codes (ENZO) but not for others (Gadget).
baryons         = yes # generate ICs for Baryons?
use_2LPT        = no # Set to yes if 2nd order Lagrangian perturbation theory shall be used to compute particle displacements and velocities
use_LLA         = no # Set to yes if the baryonic density field shall be computed using a second order expansion of the local Lagrangian approximation (LLA). See Section 5.3 in Hahn & Abel (2011)
periodic_TF     = yes

# region: box
ref_center      = 0.5, 0.5, 0.5 # relative to 0...1
ref_extent      = 0.1, 0.1, 0.1 # relative to 0...1

# region: ellipsoid
# region_point_file =
# region_point_shift =
# region_point_levelmin = 

[cosmology]
# based on Planck 2018 results: https://ui.adsabs.harvard.edu/abs/2020A%26A...641A...6P/abstract (Tab.2, 2nd-to-last column)
Omega_m  = 0.3153 # total matter density (DM+Baryons)
Omega_L  = 0.6847
w0       = -1.0
wa       = 0.0
Omega_b  = 0.04932 # Baryons
H0       = 67.36
sigma_8  = 0.8111
nspec    = 0.9649
transfer = eisenstein
YHe      = 0.248 # Helium abundance (by mass). This is used to compute the initial gas temperature if baryons are present. (Will only be used if the simulation code supports reading temperature fields). Optional. Default value: 0.248
gamma    = 1.666667 # Adiabatic exponent. This is used to compute the initial gas temperature if baryons are present. (Will only be used if the simulation code supports reading temperature fields). Optional. Default value: 5/3

[random]
# random seeds for the various levels should be given in the form 'seed[level]'
seed[7]  = 271214
seed[8]  = 190911
seed[9]  = 040484
seed[10] = 140281
seed[11] = 4711
seed[12] = 0815

[output]
##FLASH MUSIC data format
##requires HDF5 installation and HDF5 enabled in Makefile
format   = flash
filename = flash_music.hdf5

##generic MUSIC data format (used for testing)
##requires HDF5 installation and HDF5 enabled in Makefile
#format   = generic
#filename = generic.hdf5

##ENZO - also outputs the settings for the parameter file
##requires HDF5 installation and HDF5 enabled in Makefile
#format   = enzo
#filename = ic.enzo

##Gadget-2 (type=1: high-res particles, type=5: rest)
#format   = gadget2
#filename = ics_gadget.dat

##Grafic2 compatible format for use with RAMSES
##option 'ramses_nml'=yes writes out a startup nml file
#format     = grafic2
#filename   = ics_ramses
#ramses_nml = yes

##TIPSY compatible with PKDgrav and Gasoline
#format   = tipsy
#filename = ics_tipsy.dat

## NYX compatible output format
##requires boxlib installation and boxlib enabled in Makefile
#format   = nyx
#filename = init

[poisson]
fft_fine      = yes
accuracy      = 1e-5
pre_smooth    = 3
post_smooth   = 3
smoother      = gs
laplace_order = 6
grad_order    = 6
